from yaml import load
try :
  from yaml import CLoader as Loader
except :
  from yaml import Loader


import json, os
cpus = {}
inherits = {}


def read_from_file(path) :
    with open(path) as f :
        return load(f.read(), Loader=Loader)


for root, dirs, files in os.walk('SpecDB/specs') :
    for name in files :
        try :
             data = read_from_file(os.path.join(root, name))
             if 'hidden' in list(data.keys()) :
                 inherits[data['name']] = data
             if 'isPart' in list(data.keys()) and data['isPart'] :
                 if 'type' in list(data.keys()) and (data['type'] == 'CPU'  or data['type'] == 'APU'):
                     name = data['name']
                     del data['name']
                     cpus[name] = data
        except :
            continue


for inherit in inherits :
    if 'inherits' in inherits[inherit] :
        for i in inherits[inherit]['inherits'] :
            inherits[inherit]['data'].update(inherits[i]['data'])


for cpu in cpus :
    if 'inherits' in cpus[cpu] :
        cpus[cpu]['data'].update(inherits[cpus[cpu]['inherits'][0]]['data'])


path = 'amd.json'
with open(path, 'w') as f :
    json.dump(cpus, f, indent=4)
