# Primary imports
import hashlib
import inspect
# AIOHTTP
import aiohttp
# Discord Imports
from discord.ext import commands

__author__ = 'Eragon5779'
__version__ = '1.0'

class hash() :
    def __init__(self, bot) :
        self.bot = bot

    @commands.group(pass_context=True, no_pm=True)
    async def hash(self, ctx) :
        """Usage: $hash <algo> <hash_data>\n\nHashes file or text with specified algorithm. If hashing file, hash_data not needed"""
        if ctx.invoked_subcommand is None :
            await self.bot.send_cmd_help(ctx)

    async def get_data(self, algo, hash_data: str = '', hash_file = []) :
        if hash_file :
            async with aiohttp.ClientSession() as session :
                async with session.get(hash_file[0]['url']) as resp :
                    if resp.status == 200 :
                        file = await resp.read()
                        return self.text_hash(file, algo)
        elif hash_data :
            return self.text_hash(hash_data, algo)
        else :
            return None

    def text_hash(self, text, algo: str) :
        m = getattr(hashlib, algo)()
        if type(text) == str :
            text = text.encode('UTF-8')
        m.update(text)
        if 'shake' in algo :
            return m.hexdigest(128)
        return m.hexdigest()

    # SHA algorithms

    @hash.command(pass_context=True)
    async def sha1(self, ctx, *, hash_data: str = '') :
        ch = ctx.message.channel
        algo = inspect.stack()[0][3]
        h = await self.get_data(algo, hash_data, ctx.message.attachments)
        if not h :
            await self.bot.send_cmd_help(ctx)
            return
        await self.bot.send_message(ch, f'{h}')

    @hash.command(pass_context=True)
    async def sha224(self, ctx, *, hash_data: str = '') :
        ch = ctx.message.channel
        algo = inspect.stack()[0][3]
        h = await self.get_data(algo, hash_data, ctx.message.attachments)
        if not h :
            await self.bot.send_cmd_help(ctx)
            return
        await self.bot.send_message(ch, f'{h}')

    @hash.command(pass_context=True)
    async def sha256(self, ctx, *, hash_data: str = '') :
        ch = ctx.message.channel
        algo = inspect.stack()[0][3]
        h = await self.get_data(algo, hash_data, ctx.message.attachments)
        if not h :
            await self.bot.send_cmd_help(ctx)
            return
        await self.bot.send_message(ch, f'{h}')

    @hash.command(pass_context=True)
    async def sha384(self, ctx, *, hash_data: str = '') :
        ch = ctx.message.channel
        algo = inspect.stack()[0][3]
        h = await self.get_data(algo, hash_data, ctx.message.attachments)
        if not h :
            await self.bot.send_cmd_help(ctx)
            return
        await self.bot.send_message(ch, f'{h}')

    @hash.command(pass_context=True)
    async def sha512(self, ctx, *, hash_data: str = '') :
        ch = ctx.message.channel
        algo = inspect.stack()[0][3]
        h = await self.get_data(algo, hash_data, ctx.message.attachments)
        if not h :
            await self.bot.send_cmd_help(ctx)
            return
        await self.bot.send_message(ch, f'{h}')

    # SHA3 algorithms

    @hash.command(pass_context=True)
    async def sha3_224(self, ctx, *, hash_data: str = '') :
        ch = ctx.message.channel
        algo = inspect.stack()[0][3]
        h = await self.get_data(algo, hash_data, ctx.message.attachments)
        if not h :
            await self.bot.send_cmd_help(ctx)
            return
        await self.bot.send_message(ch, f'{h}')

    @hash.command(pass_context=True)
    async def sha3_256(self, ctx, *, hash_data: str = '') :
        ch = ctx.message.channel
        algo = inspect.stack()[0][3]
        h = await self.get_data(algo, hash_data, ctx.message.attachments)
        if not h :
            await self.bot.send_cmd_help(ctx)
            return
        await self.bot.send_message(ch, f'{h}')

    @hash.command(pass_context=True)
    async def sha3_384(self, ctx, *, hash_data: str = '') :
        ch = ctx.message.channel
        algo = inspect.stack()[0][3]
        h = await self.get_data(algo, hash_data, ctx.message.attachments)
        if not h :
            await self.bot.send_cmd_help(ctx)
            return
        await self.bot.send_message(ch, f'{h}')

    @hash.command(pass_context=True)
    async def sha3_512(self, ctx, *, hash_data: str = '') :
        ch = ctx.message.channel
        algo = inspect.stack()[0][3]
        h = await self.get_data(algo, hash_data, ctx.message.attachments)
        if not h :
            await self.bot.send_cmd_help(ctx)
            return
        await self.bot.send_message(ch, f'{h}')

    # SHAKE algorithms

    @hash.command(pass_context=True)
    async def shake_128(self, ctx, *, hash_data: str = '') :
        ch = ctx.message.channel
        algo = inspect.stack()[0][3]
        h = await self.get_data(algo, hash_data, ctx.message.attachments)
        if not h :
            await self.bot.send_cmd_help(ctx)
            return
        await self.bot.send_message(ch, f'{h}')

    @hash.command(pass_context=True)
    async def shake_256(self, ctx, *, hash_data: str = '') :
        ch = ctx.message.channel
        algo = inspect.stack()[0][3]
        h = await self.get_data(algo, hash_data, ctx.message.attachments)
        if not h :
            await self.bot.send_cmd_help(ctx)
            return
        await self.bot.send_message(ch, f'{h}')

    # Other algorithms

    @hash.command(pass_context=True)
    async def md5(self, ctx, *, hash_data: str = '') :
        ch = ctx.message.channel
        algo = inspect.stack()[0][3]
        h = await self.get_data(algo, hash_data, ctx.message.attachments)
        if not h :
            await self.bot.send_cmd_help(ctx)
            return
        await self.bot.send_message(ch, f'{h}')

def setup(bot) :
    bot.add_cog(hash(bot))
