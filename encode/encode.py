# Primary Imports
import base64
# Discord Imports
from discord.ext import commands

__author__  = 'Eragon5779'
__version__ = '1.0'

class encode() :
    def __init__(self, bot) :
        self.bot = bot

    @commands.group(pass_context=True, no_pm=True)
    async def encode(self, ctx) :
        """Usage: $encode <method> <data>\n\nEncodes text with specified encoding method"""
        if ctx.invoked_subcommand is None :
            await self.bot.send_cmd_help(ctx)

    @encode.command(pass_context=True)
    async def b64e(self, ctx, *, data: str) :
        await self.bot.send_message(ctx.message.channel, base64.b64encode(data.encode('UTF-8')).decode('UTF-8'))

    @encode.command(pass_context=True)
    async def b16e(self, ctx, *, data: str) :
        await self.bot.send_message(ctx.message.channel, base64.b16encode(data.encode('UTF-8')).decode('UTF-8'))

    @encode.command(pass_context=True)
    async def b32e(self, ctx, *, data: str) :
        await self.bot.send_message(ctx.message.channel, base64.b32encode(data.encode('UTF-8')).decode('UTF-8'))

    @encode.command(pass_context=True)
    async def a85e(self, ctx, *, data: str) :
        await self.bot.send_message(ctx.message.channel, base64.a85encode(data.encode('UTF-8')).decode('UTF-8'))

    @encode.command(pass_context=True)
    async def b85e(self, ctx, *, data: str) :
        await self.bot.send_message(ctx.message.channel, base64.b85encode(data.encode('UTF-8')).decode('UTF-8'))

    @commands.group(pass_context=True, no_pm=True)
    async def decode(self, ctx) :
        """Usage: $decode <method> <data>\n\nDecodes text with specified encoding method"""
        if ctx.invoked_subcommand is None :
            await self.bot.send_cmd_help(ctx)

    @decode.command(pass_context=True)
    async def b64d(self, ctx, *, data: str) :
        await self.bot.send_message(ctx.message.channel, base64.b64decode(data.encode('UTF-8')).decode('UTF-8'))

    @decode.command(pass_context=True)
    async def b16d(self, ctx, *, data: str) :
        await self.bot.send_message(ctx.message.channel, base64.b16decode(data.encode('UTF-8')).decode('UTF-8'))

    @decode.command(pass_context=True)
    async def b32d(self, ctx, *, data: str) :
        await self.bot.send_message(ctx.message.channel, base64.b32decode(data.encode('UTF-8')).decode('UTF-8'))

    @decode.command(pass_context=True)
    async def a85d(self, ctx, *, data: str) :
        await self.bot.send_message(ctx.message.channel, base64.a85decode(data.encode('UTF-8')).decode('UTF-8'))

    @decode.command(pass_context=True)
    async def b85d(self, ctx, *, data: str) :
        await self.bot.send_message(ctx.message.channel, base64.b85decode(data.encode('UTF-8')).decode('UTF-8'))


def setup(bot) :
    bot.add_cog(encode(bot))
