# TechCogs

Technology-related cogs for use with [Red-DiscordBot](https://github.com/Cog-Creators/Red-DiscordBot).

Meant to be used with Red's downloader by adding it with:

`[p]cog repo add https://gitlab.com/Eragon5779/techcogs.git`

## Cogs

### CPUCompare

A CPU specsheet lookup. Utilizes [Intel ARK](http://ark.intel.com) and [SpecDB](https://beta.specdb.info) for file creation.

** Will be updated every time CPUs are released **

#### Prerequisites
* pip packages
  * `discord`

#### Commands

* `[p]compare [brand1] [cpu1] [brand2] [cpu2]`
  * Finds and compares 2 CPUs. Prompts for missing info
* `[p]intel [arg]`
  * Finds an Intel CPU based on the search argument
* `[p]amd [arg]`
  * Finds an AMD CPU based on the search argument
* `[p]cpureload`
  * Reloads the data files after a data-only/manual update

### Hash

A wrapper for Python `hashlib`. Supports all guaranteed algorithms from the library.

Is capable of hashing both files and text data.

#### Prerequisites

* pip packages
  * `aiohttp`
  * `discord`

#### Commands

* `[p]hash <algo> <hash_data>`
  * Hashes data using the supplied algorithm and data.
  * `hash_data` not needed if file is attached.
  * Supported algorithms:
    * md5       
    * sha1      
    * sha224    
    * sha256    
    * sha384    
    * sha3_224  
    * sha3_256  
    * sha3_384  
    * sha3_512  
    * sha512    
    * shake_128
    * shake_256

### Encode

Wrapper for Python `base64` library.

Can encode and decode all methods provided by this library.

#### Prerequisites

* pip packages
  * `discord`

#### Commands


* `[p] encode <method> <data>`
  * Encodes data using specified method
  * Supported methods
    * a85
    * b16
    * b32
    * b64
    * b85
* `[p]decode <method> <data>`
  * Decodes data using specified method
  * Supported methods
    * a85
    * b16
    * b32
    * b64
    * b85   

## Utilities

### specdbdump.py

Used to dump SpecDB data into `amd.json` file. Useful for processing.

### intelreprocess.py

Takes in the raw `intel.json` and cleans it up to minimize the filesize.

## Contact Information

Discord Username: `eragon5779#5779`

Discord Server: `discord.gg/red`
