# Discord Imports
import discord
from discord.ext import commands
from discord import Embed

# Data processing imports
from .utils.dataIO import fileIO

# Logging
import logging

__author__ = 'Eragon5779'
__version__ = '1.0'

"""
Based on my earlier TechBot (https://gitlab.com/Eragon5779/TechBot)
"""

def compare(cpu1, cpu2, cpu1_name, cpu2_name) :
    """Create table for 2 CPU comparison"""
    cpu1_data, cpu2_data = {},{}
    cpu1_brand, cpu2_brand = "",""
    if "Intel" in cpu1_name :
        cpu1_data = process_intel(cpu1, True)
        cpu1_name = cpu1_name.replace("Processor ","").split("Intel®")[1].split("(")[0]
        if 'Extreme' in cpu1_name :
            cpu1_name = cpu1_name.split('Extreme')[0]
        cpu1_brand = 'Intel'
    else :
        cpu1_data = process_amd(cpu1['data'], True)
        cpu1_brand = 'AMD'
    if "Intel" in cpu2_name :
        cpu2_data = process_intel(cpu2, True)
        cpu2_name = cpu2_name.replace("Processor ","").split("Intel®")[1].split("(")[0]
        cpu2_brand = 'Intel'
    else :
        cpu2_data = process_amd(cpu2['data'], True)
        cpu2_brand = 'AMD'
    for f in cpu1_data :
        if cpu1_data[f] == None or (type(cpu1_data[f]) == str and cpu1_data[f].strip() == '') :
            cpu1_data[f] = 'N/A'
    for f in cpu2_data :
        if cpu2_data[f] == None or (type(cpu2_data[f]) == str and cpu2_data[f].strip() == '') :
            cpu2_data[f] = 'N/A'

    # Assemble the embed.
    embed = Embed(title="Comparison", color=0xffff00)
    embed.add_field(value="CPU", name="Brand", inline=True)
    embed.add_field(value=cpu1_name, name=cpu1_brand, inline=True)
    embed.add_field(value=cpu2_name, name=cpu2_brand, inline=True)
    embed.add_field(name="Cores", value="Threads", inline=True)
    embed.add_field(name=cpu1_data['cores'], value=cpu1_data['threads'], inline=True)
    embed.add_field(name=cpu2_data['cores'], value=cpu2_data['threads'], inline=True)
    embed.add_field(name="Base Clock", value="Boost Clock", inline=True)
    embed.add_field(name=cpu1_data['base_clock'], value=cpu1_data['boost_clock'], inline=True)
    embed.add_field(name=cpu2_data['base_clock'], value=cpu2_data['boost_clock'], inline=True)
    embed.add_field(value="Size", name="Cache", inline=True)
    embed.add_field(value=cpu1_data['cache'], name="L3", inline=True)
    embed.add_field(value=cpu2_data['cache'], name="L3", inline=True)
    embed.add_field(name="TDP", value="TDie", inline=True)
    embed.add_field(name=cpu1_data['tdp'], value="N/A", inline=True)
    embed.add_field(name=cpu2_data['tdp'], value="N/A", inline=True)
    embed.add_field(name="Status", value="Date", inline=True)
    embed.add_field(value=cpu1_data['release_date'], name="Released", inline=True)
    embed.add_field(value=cpu2_data['release_date'], name="Released", inline=True)
    return embed

def process_intel(data, compare=False) :

    wantedData = {
        'Cores':data['CoreCount'] if 'CoreCount' in data.keys() else 'N/A',
        'Threads':data['ThreadCount'] if 'ThreadCount' in data.keys() else 'N/A',
        'Base Clock':data['ClockSpeed'] if 'ClockSpeed' in data.keys() else 'N/A',
        'Boost Clock':data['ClockSpeedMax'] if 'ClockSpeedMax' in data.keys() else 'N/A',
        'Cache':data['Cache'] if 'Cache' in data.keys() else 'N/A',
        'TDP':data['MaxTDP'] if 'MaxTDP' in data.keys() else 'N/A',
        'Release Date':data['BornOnDate'] if 'BornOnDate' in data.keys() else 'N/A',
        'Max Memory':data['MaxMem'] if 'MaxMem' in data.keys() else 'N/A',
        'Lithography':data['Lithography'] if 'Lithography' in data.keys() else 'N/A',
        'Socket':data['SocketsSupported'] if 'SocketsSupported' in data.keys() else 'N/A',
        'Multiple CPU':data['ScalableSockets'] if 'ScalableSockets' in data.keys() else 'N/A'
    }
    if compare :
        wantedData = {
            'cores':data['CoreCount'] if 'CoreCount' in data.keys() else 'N/A',
            'threads':data['ThreadCount'] if 'ThreadCount' in data.keys() else 'N/A',
            'base_clock':data['ClockSpeed'] if 'ClockSpeed' in data.keys() else 'N/A',
            'boost_clock':data['ClockSpeedMax'] if 'ClockSpeedMax' in data.keys() else 'N/A',
            'cache':data['Cache'] if 'Cache' in data.keys() else 'N/A',
            'tdp':data['MaxTDP'] if 'MaxTDP' in data.keys() else 'N/A',
            'release_date':data['BornOnDate'] if 'BornOnDate' in data.keys() else 'N/A',
        }
    return wantedData


def process_amd(data,compare=False) :
    wantedData = {
        'Cores':data['Core Count'] if 'Core Count' in data.keys() else 'N/A',
        'Threads':data['Thread Count'] if 'Thread Count' in data.keys() else 'N/A',
        'Base Clock':data['Base Frequency'] if 'Base Frequency' in data.keys() else 'N/A',
        'Boost Clock':data['Boost Frequency'] if 'Boost Frequency' in data.keys() else 'N/A',
        'Cache':data['L3 Cache (Total)'] if 'L3 Cache (Total)' in data.keys() else 'N/A',
        'TDP':data['TDP'] if 'TDP' in data.keys() else 'N/A',
        'Release Date':data['Release Date'] if 'Release Date' in data.keys() else 'N/A',
        'Lithography':data['Lithography'] if 'Lithography' in data.keys() else 'N/A',
        'Socket':data['Socket'] if 'Socket' in data.keys() else 'N/A'
    }
    if compare :
        wantedData = {
            'cores':data['Core Count'] if 'Core Count' in data.keys() else 'N/A',
            'threads':data['Thread Count'] if 'Thread Count' in data.keys() else 'N/A',
            'base_clock':data['Base Frequency'] if 'Base Frequency' in data.keys() else 'N/A',
            'boost_clock':data['Boost Frequency'] if 'Boost Frequency' in data.keys() else 'N/A',
            'cache':data['L3 Cache (Total)'] if 'L3 Cache (Total)' in data.keys() else 'N/A',
            'tdp':data['TDP'] if 'TDP' in data.keys() else 'N/A',
            'release_date':data['Release Date'] if 'Release Date' in data.keys() else 'N/A',
        }
    return wantedData

def amd_search(search) :
    """Search through AMD specs file for needed data"""
    global amd
    matching = []
    cpuList = ""
    count = 0
    for cpu in amd.keys() :
        if search.lower() in amd[cpu]['humanName'].lower():
            matching.append(cpu)
    matching.sort()
    for cpu in matching :
        cpuList += '[%d] %s\n' % (count, amd[cpu]['humanName'])
        count += 1
    return matching, cpuList, count

def intel_search(search) :
    """Search through Intel specs file for needed data"""
    global intel
    matching = []
    cpuList = ""
    count = 0
    for cpu in intel.keys() :
        if search.lower() in cpu.lower() :
            matching.append(cpu)
    matching.sort()
    for cpu in matching :
        cpuList += '[%d] %s\n' % (count, cpu)
        count += 1
    return matching, cpuList, count

class CPUCompare :
    def __init__(self, bot) :
        self.bot = bot

    @commands.command(pass_context=True, name="cpureload")
    async def _cpu_reload(self, ctx) :
        """Reloads CPU data if manual update done"""
        global intel, amd
        intel = fileIO("data/cpucompare/intel.json", IO="load")
        amd = fileIO("data/cpucompare/amd.json", IO="load")
        await self.bot.send_message(ctx.message.channel, "Updated files successfully")


    @commands.command(pass_context=True, name="intel")
    async def _intel(self, ctx, *, arg="") :
        """Retrieves requested Intel CPU and returns an embed with the specs"""
        search = ''
        if len(ctx.message.content) > 6 :
            search = arg
        else :
            await self.bot.send_message(ctx.message.channel, "Please enter model you want to search for (i.e. 9700)")
            msg = await self.bot.wait_for_message(author=ctx.message.author)
            search = msg.content
        matching, cpuList, count = intel_search(search)
        if len(cpuList) < 1 :
            await self.bot.send_message(ctx.message.channel, "No processors found")
            return
        try :
            await self.bot.send_message(ctx.message.channel, cpuList)
        except :
            await self.bot.send_message(ctx.message.channel, 'Error: List too large. Please refine search')
            return
        choice = 0
        if count > 1 :
            await self.bot.send_message(ctx.message.channel, 'Which CPU do you want?')
            msg = await self.bot.wait_for_message(author=ctx.message.author)
            try :
                choice = int(msg.content)
                if choice > count or choice < 0 :
                    raise ValueError('Out of Range')
            except ValueError :
                await self.bot.send_message(ctx.message.channel, "Error: Choice out of range. Please restart your search")
                return
            except :
                await self.bot.send_message(ctx.message.channel, "Error: Not a number. Please restart your search.")
                return

        cpu_data = intel[matching[choice]]
        cpu_name = matching[choice].replace("Processor ","").split("Intel®")[1].split("(")[0].strip()
        embed = discord.Embed(title=cpu_name, color=0x0000ff)
        wantedData = process_intel(cpu_data)
        for field in wantedData.keys() :
            if type(wantedData[field]) == str and wantedData[field].strip() == '' :
                wantedData[field] = 'N/A'
            elif not wantedData[field] :
                wantedData[field] = 'N/A'
            embed.add_field(name=field, value=wantedData[field], inline=True)
        await self.bot.send_message(ctx.message.channel, embed=embed)

    @commands.command(pass_context=True, name="amd")
    async def _amd(self, ctx, *, arg="") :
        """Retrieves requested AMD CPU and returns an embed with the specs"""
        search = ''
        if len(ctx.message.content) > 6 :
            #search = ctx.message.content.split(' ')[1]
            search = arg
        else :
            await self.bot.send_message(ctx.message.channel, 'Please enter model you want to search for (i.e. 2700)')

            msg = await self.bot.wait_for_message(author=ctx.message.author)
            search = msg.content

        matching, cpuList, count = amd_search(search=search)

        if len(cpuList) < 1 :
            await self.bot.send_message(ctx.message.channel, "No processors found")
            return
        try :
            await self.bot.send_message(ctx.message.channel, cpuList)
        except :
            await self.bot.send_message(ctx.message.channel, 'Error: List too large. Please refine search.')
            return
        choice = 0
        if count > 1 :
            await self.bot.send_message(ctx.message.channel, 'Which CPU do you want?')
            msg = await self.bot.wait_for_message(author=ctx.message.author)
            try :
                choice = int(msg.content)
                if choice > count or choice < 0 :
                    raise ValueError('Out of Range')
            except ValueError :
                await self.bot.send_message(ctx.message.channel, "Error: Out of Range. Please restart your search.")
            except :
                await self.bot.send_message(ctx.message.channel, "Error: Not a number. Please restart your search.")
                return
        cpu_data = amd[matching[choice]]['data']
        wantedData = process_amd(cpu_data)
        embed = discord.Embed(title=matching[choice], color=0xff0000)
        for field in wantedData.keys() :
            if type(wantedData[field]) == str and wantedData[field].strip() == '' :
                wantedData[field] = 'N/A'
            elif not wantedData[field] :
                wantedData[field] = 'N/A'
            embed.add_field(name=field, value=wantedData[field], inline=True)
        await self.bot.send_message(ctx.message.channel, embed=embed)

    @commands.command(pass_context=True, name="compare")
    async def _compare(self, ctx, brand1=None, cpu1=None, brand2=None, cpu2=None) :
        """Compares 2 specified CPUs"""
        # Constants for easy lookup
        searches = {
            "amd":amd_search,
            "intel":intel_search
        }
        lists = {
            "amd":amd,
            "intel":intel
        }

        # Arg sanitation
        if brand1 and brand1.lower() not in ['intel','amd'] :
            brand1 = None
        if cpu1 and cpu1.lower() in ['intel','amd'] :
            if brand1 :
                brand2 = cpu1
            else :
                brand1 = cpu1
            cpu1 = None
        if brand2 and brand2.lower() not in ['intel','amd'] :
            brand1 = None
        if cpu2 and cpu2.lower() in ['intel','amd'] :
            if brand1 :
                brand2 = cpu2
            else :
                brand1 = cpu2
            cpu2 = None

        # CPU 1 search
        if not brand1 :
            await self.bot.send_message(ctx.message.channel, "Which brand would you like for the first CPU? (Intel or AMD)")
            msg = await self.bot.wait_for_message(author=ctx.message.author)
            brand1 = msg.content.lower()
        if brand1.lower() not in ['intel','amd'] :
            await self.bot.send_message(ctx.message.channel, f"{brand1} is not supported. Please try again with Intel/AMD.")
            return
        if not cpu1 :
            await self.bot.send_message(ctx.message.channel, f"Please enter search term for {brand1}: ")
            msg = await self.bot.wait_for_message(author=ctx.message.author)
            cpu1 = msg.content

        brand1_lower = brand1.lower()

        matching, cpuList, count = searches[brand1_lower](cpu1)
        if len(cpuList) < 1 :
            await self.bot.send_message(ctx.message.channel, f"{brand1} {cpu1} not found")
            return
        try :
            await self.bot.send_message(ctx.message.channel, cpuList)
        except :
            await self.bot.send_message(ctx.message.channel, 'Error: List too large. Please refine search.')
            return
        choice = 0

        if count > 1 :
            await self.bot.send_message(ctx.message.channel, 'Which CPU do you want?')
            msg = await self.bot.wait_for_message(author=ctx.message.author)
            try :
                choice = int(msg.content)
                if choice > count or choice < 0 :
                    raise ValueError('Out of Range')
            except ValueError :
                await self.bot.send_message(ctx.message.channel, "Error: Out of Range. Please restart your search.")
            except :
                await self.bot.send_message(ctx.message.channel, "Error: Not a number. Please restart your search.")
                return

        cpu1 = lists[brand1_lower][matching[choice]]
        cpu1_name = matching[choice]

        # CPU 2 search
        if not brand2 :
            await self.bot.send_message(ctx.message.channel, "Which brand would you like for the first CPU? (Intel or AMD)")
            msg = await self.bot.wait_for_message(author=ctx.message.author)
            brand2 = msg.content.lower()
        if brand2.lower() not in ['intel','amd'] :
            await self.bot.send_message(ctx.message.channel, f"{brand2} is not supported. Please try again with Intel/AMD.")
            return
        if not cpu2 :
            await self.bot.send_message(ctx.message.channel, f"Please enter search term for {brand2}: ")
            msg = await self.bot.wait_for_message(author=ctx.message.author)
            cpu2 = msg.content

        brand2_lower = brand2.lower()

        matching, cpuList, count = searches[brand2_lower](cpu2)
        if len(cpuList) < 1 :
            await self.bot.send_message(ctx.message.channel, f"{brand2} {cpu2} not found")
            return
        try :
            await self.bot.send_message(ctx.message.channel, cpuList)
        except :
            await self.bot.send_message(ctx.message.channel, 'Error: List too large. Please refine search.')
            return
        choice = 0

        if count > 1 :
            await self.bot.send_message(ctx.message.channel, 'Which CPU do you want?')
            msg = await self.bot.wait_for_message(author=ctx.message.author)
            try :
                choice = int(msg.content)
                if choice > count or choice < 0 :
                    raise ValueError('Out of Range')
            except ValueError :
                await self.bot.send_message(ctx.message.channel, "Error: Out of Range. Please restart your search.")
            except :
                await self.bot.send_message(ctx.message.channel, "Error: Not a number. Please restart your search.")
                return

        cpu2 = lists[brand2_lower][matching[choice]]
        cpu2_name = matching[choice]

        tbl = compare(cpu1, cpu2, cpu1_name, cpu2_name)

        await self.bot.send_message(ctx.message.channel, embed=tbl)

def setup(bot) :
    global intel, amd, logger
    logger = logging.getLogger('cpucompare')
    if logger.level == 0 :
        logger.setLevel(logging.INFO)
        handler = logging.FileHandler(filename='data/cpucompare/comparisons.log', encoding='utf-8', mode='a')
        handler.setFormatter(logging.Formatter('%(asctime)s %(message)s', datefmt="[%d/%m/%Y %H:%M]"))
        logger.addHandler(handler)
    intel = fileIO("data/cpucompare/intel.json", IO="load")
    amd = fileIO("data/cpucompare/amd.json", IO="load")
    n = CPUCompare(bot)
    bot.add_cog(n)
